<?php
function add_child_template_support() {
	 /*add theme colors*/
	add_theme_support(
		'editor-color-palette',
		array(
			array(
				'name'  => __( 'Primary', 'wp-theme-parent' ),
				'slug'  => 'primary',
				'color' => '#3A1C00',
			),
			array(
				'name'  => __( 'Secondary', 'wp-theme-parent' ),
				'slug'  => 'secondary',
				'color' => '#EC971F',
			),
			array(
				'name'  => __( 'Tertiary', 'wp-theme-parent' ),
				'slug'  => 'tertiary',
				'color' => '#096309',
			),
			array(
				'name'  => __( 'White', 'wp-theme-parent' ),
				'slug'  => 'white',
				'color' => '#ffffff',
			),
			array(
				'name'  => __( 'Dark', 'wp-theme-parent' ),
				'slug'  => 'dark',
				'color' => '#333333',
			),
			array(
				'name'  => __( 'Light Gray', 'wp-theme-parent' ),
				'slug'  => 'light-gray',
				'color' => '#f7f7f7',
			),
		)
	);

	// add support for editor styles
	add_theme_support( 'editor-styles' );
}

add_action( 'after_setup_theme', 'add_child_template_support', 20 );
