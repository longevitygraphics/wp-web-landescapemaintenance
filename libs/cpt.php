<?php
/**
 * Functions file that defines CPT for theme
 *
 * @package landescape
 */

?>

<?php
/**
 * Init Custom Post Types for Theme
 */
function lg_template_init_cpts() {
	$lg_projects_labels = array(
		'name'                  => _x( 'Project', 'Post type general name', 'landescape' ),
		'singular_name'         => _x( 'Project', 'Post type singular name', 'landescape' ),
		'menu_name'             => _x( 'Projects', 'Admin Menu text', 'landescape' ),
		'name_admin_bar'        => _x( 'Projects', 'Add New on Toolbar', 'landescape' ),
		'add_new'               => __( 'Add New', 'landescape' ),
		'add_new_item'          => __( 'Add New Project', 'landescape' ),
		'new_item'              => __( 'New Project', 'landescape' ),
		'edit_item'             => __( 'Edit Project', 'landescape' ),
		'view_item'             => __( 'View Project', 'landescape' ),
		'all_items'             => __( 'Projects', 'landescape' ),
		'search_items'          => __( 'Search Projects', 'landescape' ),
		'parent_item_colon'     => __( 'Parent Project:', 'landescape' ),
		'not_found'             => __( 'No projects found.', 'landescape' ),
		'not_found_in_trash'    => __( 'No projects found in Trash.', 'landescape' ),
		'featured_image'        => _x( 'Project Cover Image', 'Overrides the "Featured Image" phrase for this post type. Added in 4.3', 'landescape' ),
		'set_featured_image'    => _x( 'Set cover image', 'Overrides the "Set featured image" phrase for this post type. Added in 4.3', 'landescape' ),
		'remove_featured_image' => _x( 'Remove cover image', 'Overrides the "Remove featured image" phrase for this post type. Added in 4.3', 'landescape' ),
		'use_featured_image'    => _x( 'Use as cover image', 'Overrides the "Use as featured image" phrase for this post type. Added in 4.3', 'landescape' ),
		'archives'              => _x( 'Project archives', 'The post type archive label used in nav menus. Default "Post Archives". Added in 4.4', 'landescape' ),
		'insert_into_item'      => _x( 'Insert into Project', 'Overrides the "Insert into post"/"Insert into page" phrase (used when inserting media into a post). Added in 4.4', 'landescape' ),
		'uploaded_to_this_item' => _x( 'Uploaded to this Project', 'Overrides the "Uploaded to this post"/"Uploaded to this page" phrase (used when viewing media attached to a post). Added in 4.4', 'landescape' ),
		'filter_items_list'     => _x( 'Filter Project list', 'Screen reader text for the filter links heading on the post type listing screen. Default "Filter posts list"/"Filter pages list". Added in 4.4', 'landescape' ),
		'items_list_navigation' => _x( 'Project list navigation', 'Screen reader text for the pagination heading on the post type listing screen. Default "Posts list navigation"/"Pages list navigation". Added in 4.4', 'landescape' ),
		'items_list'            => _x( 'Project list', 'Screen reader text for the items list heading on the post type listing screen. Default "Posts list"/"Pages list". Added in 4.4', 'landescape' ),
	);

	$lg_projects_args = array(
		'labels'             => $lg_projects_labels,
		'description'        => 'Project Custom Post Type.',
		'public'             => true,
		'publicly_queryable' => true,
		'show_ui'            => true,
		'show_in_menu'       => 'edit.php',
		'query_var'          => true,
		'rewrite'            => array( 'slug' => 'project' ),
		'capability_type'    => 'post',
		'has_archive'        => false,
		'hierarchical'       => true,
		'menu_position'      => 20,
		'supports'           => array( 'title', 'editor', 'thumbnail', 'excerpt' ),
		'taxonomies'         => array( 'lg-project-category' ),
		'show_in_rest'       => true,
		'menu_icon'          => 'dashicons-hammer',
	);

	register_post_type( 'lg-project', $lg_projects_args );

	register_taxonomy(
		'lg-project-category',
		'project',
		array(
			'label'        => __( 'Project Category' ),
			'rewrite'      => array(
				'slug'       => 'lg-project-category',
				'with_front' => false,
			),
			'hierarchical' => true,
			'show_in_rest' => true,
		)
	);
}
add_action( 'init', 'lg_template_init_cpts' );