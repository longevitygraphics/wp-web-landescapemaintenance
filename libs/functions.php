<?php

function register_footer_widgets () {
	// Footer Delta
	register_sidebar(
		array(
			'name'          => esc_html__( 'Footer Delta', '_s' ),
			'id'            => 'footer-delta',
			'description'   => esc_html__( 'Add widgets here.', '_s' ),
			'before_widget' => '<section id="%1$s" class="widget %2$s">',
			'after_widget'  => '</section>',
			'before_title'  => '<h2 class="widget-title">',
			'after_title'   => '</h2>',
		)
	);
}

add_action( 'widgets_init', 'register_footer_widgets' );

