<div class="main-navigation">
	<div class="container">
		<div class="header-top">
			<a class="navbar-brand mr-auto mr-lg-0" href="<?php echo get_site_url() ?>"><?php echo site_logo(); ?></a>
			<div class="header-top__right">
				<a href="tel:<?php echo do_shortcode( "[lg-phone-main]" ) ?>">
					<div class="icon-wrap">
						<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 33.63 34.49">
							<g id="Layer_2" data-name="Layer 2">
								<g id="Layer_1-2" data-name="Layer 1">
									<path
										d="M26.8,15.26a7.56,7.56,0,0,0-.22-3.36,7.89,7.89,0,0,0-2-3.34,6.14,6.14,0,0,0-3.34-1.77A5,5,0,0,0,19,6.93"
										style="fill:none;stroke:#3a1c00;stroke-linecap:round;stroke-linejoin:round;stroke-width:1.440000057220459px"/>
									<path
										d="M32.26,17.93a13.28,13.28,0,0,0,0-8.25,13.62,13.62,0,0,0-3.09-5.2,12.15,12.15,0,0,0-5-3.22,10.76,10.76,0,0,0-6.78,0"
										style="fill:none;stroke:#3a1c00;stroke-linecap:round;stroke-linejoin:round;stroke-width:1.440000057220459px"/>
									<path d="M9.6,12c-1.75,1.7-1.22,5.52,1.27,8.78s6.05,4.75,8.14,3.51"
										  style="fill:none;stroke:#3a1c00;stroke-linecap:round;stroke-linejoin:round;stroke-width:1.440000057220459px"/>
									<path
										d="M24.69,31.65a13.07,13.07,0,0,1-2,1.56c-2,1.47-5.88,0-10.06-3.65a41,41,0,0,1-5.54-6C1.21,15.85-.8,8.25,1.91,6c1.46-1.2,1.86-1.47,2-1.48"
										style="fill:none;stroke:#3a1c00;stroke-linecap:round;stroke-linejoin:round;stroke-width:1.440000057220459px"/>
									<path
										d="M25.29,30.59l-5.48-7.15A1,1,0,0,1,20,22.12l2.48-1.9a.94.94,0,0,1,1.32.17l5.48,7.15a1,1,0,0,1-.18,1.32l-2.48,1.9a.94.94,0,0,1-1.32-.17Z"
										style="fill:none;stroke:#3a1c00;stroke-linecap:round;stroke-linejoin:round;stroke-width:1.440000057220459px"/>
									<path
										d="M10.56,11.37,5.08,4.21A.94.94,0,0,1,5.26,2.9L7.74,1a1,1,0,0,1,1.32.18l5.48,7.15a1,1,0,0,1-.18,1.32l-2.48,1.9a1,1,0,0,1-1.32-.17Z"
										style="fill:none;stroke:#3a1c00;stroke-linecap:round;stroke-linejoin:round;stroke-width:1.440000057220459px"/>
								</g>
							</g>
						</svg>
						<div>
							<span>Call us today!</span>
							<span><?php echo do_shortcode( "[lg-phone-main]" ) ?></span>
						</div>
					</div>


				</a>
				<a href="mailto:<?php echo do_shortcode( "[lg-email]" ) ?>">
					<div class="icon-wrap">
						<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 34.32 33.58">
							<g id="Layer_2" data-name="Layer 2">
								<g id="Layer_1-2" data-name="Layer 1">
									<polygon
										points="0.72 32.86 33.6 32.86 33.6 12.77 17.16 0.72 0.72 12.77 0.72 32.86 0.72 32.86"
										style="fill:none;stroke:#3a1c00;stroke-linecap:round;stroke-linejoin:round;stroke-width:1.440000057220459px"/>
									<polyline points="33.6 32.86 17.16 19.39 0.72 32.86"
											  style="fill:none;stroke:#3a1c00;stroke-linecap:round;stroke-linejoin:round;stroke-width:1.440000057220459px"/>
									<line x1="1.07" y1="13.25" x2="13.03" y2="22.32"
										  style="fill:none;stroke:#3a1c00;stroke-linecap:round;stroke-linejoin:round;stroke-width:1.440000057220459px"/>
									<line x1="33.26" y1="13.25" x2="21.29" y2="22.32"
										  style="fill:none;stroke:#3a1c00;stroke-linecap:round;stroke-linejoin:round;stroke-width:1.440000057220459px"/>
								</g>
							</g>
						</svg>
						<div>
							<span>Have a question?</span>
							<span>Email Us</span>
						</div>
					</div>
				</a>
			</div>
		</div>
	</div>

	<nav class="navbar navbar-expand-xl">
		<button class="navbar-toggler hamburger hamburger--squeeze js-hamburger" type="button" data-toggle="offcanvas">
			<div class="hamburger-box">
				<div class="hamburger-inner"></div>
			</div>
		</button>


		<!-- Main Menu  -->
		<div class="navbar-collapse offcanvas-collapse" id="main-navbar">
			<div class="container">
				<?php

				$mainMenu = array(
					// 'menu'              => 'menu-1',
					'theme_location' => 'top-nav',
					'depth'          => 2,
					'container'      => false,
					/*'container' => 'div',
					'container_class' => 'navbar-collapse offcanvas-collapse bg-primary',
					'container_id' => 'main-navbar',*/
					'menu_class'     => 'navbar-nav',
					'fallback_cb'    => 'WP_Bootstrap_Navwalker::fallback',
					'walker'         => new WP_Bootstrap_Navwalker()
				);
				wp_nav_menu( $mainMenu );

				?>
			</div>

		</div>
	</nav>
</div>

