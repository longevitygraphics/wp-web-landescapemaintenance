<?php
/**
 * Product Single Template file
 * @phpcs:disable Squiz.ControlStructures.ControlSignature.NewlineAfterOpenBrace
 *
 * @package landescape
 */

?>
<?php

get_header();
?>

	<main>
	<?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>

				<?php the_content(); ?>

				<?php
		endwhile;
	endif;
	?>
	</main>

<?php get_footer(); ?>