/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, { enumerable: true, get: getter });
/******/ 		}
/******/ 	};
/******/
/******/ 	// define __esModule on exports
/******/ 	__webpack_require__.r = function(exports) {
/******/ 		if(typeof Symbol !== 'undefined' && Symbol.toStringTag) {
/******/ 			Object.defineProperty(exports, Symbol.toStringTag, { value: 'Module' });
/******/ 		}
/******/ 		Object.defineProperty(exports, '__esModule', { value: true });
/******/ 	};
/******/
/******/ 	// create a fake namespace object
/******/ 	// mode & 1: value is a module id, require it
/******/ 	// mode & 2: merge all properties of value into the ns
/******/ 	// mode & 4: return value when already ns object
/******/ 	// mode & 8|1: behave like require
/******/ 	__webpack_require__.t = function(value, mode) {
/******/ 		if(mode & 1) value = __webpack_require__(value);
/******/ 		if(mode & 8) return value;
/******/ 		if((mode & 4) && typeof value === 'object' && value && value.__esModule) return value;
/******/ 		var ns = Object.create(null);
/******/ 		__webpack_require__.r(ns);
/******/ 		Object.defineProperty(ns, 'default', { enumerable: true, value: value });
/******/ 		if(mode & 2 && typeof value != 'string') for(var key in value) __webpack_require__.d(ns, key, function(key) { return value[key]; }.bind(null, key));
/******/ 		return ns;
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";
/******/
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = "./assets/entry.js");
/******/ })
/************************************************************************/
/******/ ({

/***/ "./assets/entry.js":
/*!*************************!*\
  !*** ./assets/entry.js ***!
  \*************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

__webpack_require__(/*! ./src/js/script */ "./assets/src/js/script.js");
/*require('./src/images/mail.svg');
require('./src/images/location.svg');
require('./src/images/phone.svg');*/


__webpack_require__(/*! ./src/images/retaining-walls.svg */ "./assets/src/images/retaining-walls.svg");

__webpack_require__(/*! ./src/images/cedar-fencing.svg */ "./assets/src/images/cedar-fencing.svg");

__webpack_require__(/*! ./src/images/driveway-patio-paving.svg */ "./assets/src/images/driveway-patio-paving.svg");

__webpack_require__(/*! ./src/images/lawn-garden-maintenance.svg */ "./assets/src/images/lawn-garden-maintenance.svg");

__webpack_require__(/*! ./src/images/tree-shrub-groundcare.svg */ "./assets/src/images/tree-shrub-groundcare.svg");

__webpack_require__(/*! ./src/images/landscape-lighting.svg */ "./assets/src/images/landscape-lighting.svg");

__webpack_require__(/*! ./src/images/leaf-debris-cleanup.svg */ "./assets/src/images/leaf-debris-cleanup.svg");

__webpack_require__(/*! ./src/images/snow-removal.svg */ "./assets/src/images/snow-removal.svg");

__webpack_require__(/*! ./src/images/Leaf-Bulletpoint.svg */ "./assets/src/images/Leaf-Bulletpoint.svg");

__webpack_require__(/*! ./src/images/location-icon.png */ "./assets/src/images/location-icon.png");

__webpack_require__(/*! ./src/images/email-icon.png */ "./assets/src/images/email-icon.png");

__webpack_require__(/*! ./src/images/phone-icon.png */ "./assets/src/images/phone-icon.png");

/***/ }),

/***/ "./assets/src/images/Leaf-Bulletpoint.svg":
/*!************************************************!*\
  !*** ./assets/src/images/Leaf-Bulletpoint.svg ***!
  \************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = (__webpack_require__.p + "../images/Leaf-Bulletpoint.svg");

/***/ }),

/***/ "./assets/src/images/cedar-fencing.svg":
/*!*********************************************!*\
  !*** ./assets/src/images/cedar-fencing.svg ***!
  \*********************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = (__webpack_require__.p + "../images/cedar-fencing.svg");

/***/ }),

/***/ "./assets/src/images/driveway-patio-paving.svg":
/*!*****************************************************!*\
  !*** ./assets/src/images/driveway-patio-paving.svg ***!
  \*****************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = (__webpack_require__.p + "../images/driveway-patio-paving.svg");

/***/ }),

/***/ "./assets/src/images/email-icon.png":
/*!******************************************!*\
  !*** ./assets/src/images/email-icon.png ***!
  \******************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAADIAAAAyCAYAAAAeP4ixAAAAGXRFWHRTb2Z0d2FyZQBBZG9iZSBJbWFnZVJlYWR5ccllPAAAAflJREFUeNrsmYttgzAQQHEmYARGYISMwAjpBnSCegOPQDdgBLoB2SDdIHQC15Zs6pz8OZuU4sonIZpP4Z7v+UeqqkSJEiVKlEgPYr7gnNfi1GaU/0IIuT6ACAgmTn2GxfgQxwtREFSc3jI266pB7uIktVrE8SqOzwySr1Xjt1qplv8Ey6kMIt9GJ35SZDq+shqpCFnNOf2b8VdU5WyodZevM8m7FsekE4cgOujBIVrV6DwEwhVtfUCI3pYsBJkBqfy7PZBKI8h/cIFQVbYZ/EN/AJVuoIEv6jM7iEE/AJjxL1SzqDSblnhBjC9dgGq3vVTzqFSD74VBjLLuqppFJec90SB7q2ZRyWtBFEjqTRJUim6sJJDYskeqlKRvMojRenOoIyKvBQcUHlPlrSDMsRqYsUk4VDKv0/waiFr/w0ow12QVqRK1rCq6p4PIi7puhB3zHSqtyyC4osVs9mKHX4YpvW8Wdqg0OWApVjXszG7r1AyhDWxxalMpcJ0Oo1oQRL0f7axHkejNm6tPokFiShtIhD5jj+NT2wXCYjsbcis9bt11OlQ7Y3aIaJV2fvwzY3aIm1TaEYhhQAZ+wL16SLWHnq9m6MNDGDDrvKSf/Y7i1FX5xqJBZBWmKq/fRlYI+eAd/tAjF3tNZiDv5jPgEiVKlChRIiW+BRgADk3vaJly0KgAAAAASUVORK5CYII=");

/***/ }),

/***/ "./assets/src/images/landscape-lighting.svg":
/*!**************************************************!*\
  !*** ./assets/src/images/landscape-lighting.svg ***!
  \**************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = (__webpack_require__.p + "../images/landscape-lighting.svg");

/***/ }),

/***/ "./assets/src/images/lawn-garden-maintenance.svg":
/*!*******************************************************!*\
  !*** ./assets/src/images/lawn-garden-maintenance.svg ***!
  \*******************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = (__webpack_require__.p + "../images/lawn-garden-maintenance.svg");

/***/ }),

/***/ "./assets/src/images/leaf-debris-cleanup.svg":
/*!***************************************************!*\
  !*** ./assets/src/images/leaf-debris-cleanup.svg ***!
  \***************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = (__webpack_require__.p + "../images/leaf-debris-cleanup.svg");

/***/ }),

/***/ "./assets/src/images/location-icon.png":
/*!*********************************************!*\
  !*** ./assets/src/images/location-icon.png ***!
  \*********************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAADIAAAAyCAYAAAAeP4ixAAAAGXRFWHRTb2Z0d2FyZQBBZG9iZSBJbWFnZVJlYWR5ccllPAAAAvlJREFUeNrUWotxpDAMXaeBUIJLoINQAumADo4O4lSwcxVsOthcBeQqYFMB6QA64MyMYTw6f2Rb9iae0SxDjPCTZOlZhJ0Ix7qulfxppNRSntTt7Xq7/6VkkfIp5YMx9nH6TkMC6KRc1/AxS7lIaVLXwFIByJ8XKdzw583yN8N926I377wW9ZIEwKUMBgtflXe45/laSi9lNOg4qxDNDqJVIaGPi2/xHlAwLDeAde69oI8hFoBBdyNlAvunLgHinOEdFfAOLRhlLX10mcP3AsBwKivNpUBYwIwUCq8p4aS82cSECMiMgiqkxgAP2lLrniA6pC6uRcMcnZaBRRrE/N6Qmm1jCtAZ7xWV3w8rBsa0Tj+Eqj3CQmM6hO4p2iuqwqK8YcgywhN65xAwwCtdKJDdChOi0gfnfUNd4h7wBw0K5VKoTAWqcR1orB4bvtp+nWOreIucJwgSissrwmWwB8tzHNBr23jSrt8is/xv7bp1zNPXUWGBPB4HFsYWh/LdMjc57yvqQMTYu8UwrtFggWBj/QCSSCBuNktrgJ0HrgciarTc+XkrkCVwAalUu0YkhToGyGdgSEQDUQvcQ+qvY2pleC/eI56q/md/SQK9/6VdvyO9tmCtpLPeHllxp1AeBPicj0EcNCjU5diKK0DToELqrwAraD3z56hDFmCqPh40AjAc4fEJy58An+tTmg0Cwc0mQ4uo3T2k5rSGfpjXi1gaQ3IOUJ4ZAlumZ4TeJor5OrxyDXhm9AAYsP1eoKuJ7v2qbLK78xlwI2e4KU7EQf5H8zIV0i97apbPPVM1IGaqzmLEeysKpYK0xxTeS2splesb+ZIZyJitNVuq40jeYURQipXiS5PjDD9n/U4CUjJZtzyX3pCeV7LllKeLN8qNsRwLxgCiP5UeILsMkQlkLJUNsyzk24AAzHcOXRCoS+Na4ksu9Wal2l+lakyHAFGMu6XUgv8K5l1qBXVh+1EgHAWzK/l5O2fB/JkgLCk2y39L3KNgZi14/wQYAJOA0C+01YcrAAAAAElFTkSuQmCC");

/***/ }),

/***/ "./assets/src/images/phone-icon.png":
/*!******************************************!*\
  !*** ./assets/src/images/phone-icon.png ***!
  \******************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAADIAAAAyCAYAAAAeP4ixAAAAGXRFWHRTb2Z0d2FyZQBBZG9iZSBJbWFnZVJlYWR5ccllPAAAAnNJREFUeNrUWoFxwyAMxJ3AI3iDegR3gnoEb1C6gTfICKQTOJkg2YBs4GzgbkChp7ScijE+p0jRHRdMIKe39I+wIwSRGWMq2ybbRtt6d73l9wpCIMp+dGj4YNuxKIo9AHPff9pW2na242dWQKyTzrERHAyZc/5q27ttDfS/xyyYi+BiFkhnfm0H19qE7eTmwLpecDLkdOWN1wBsCgByPOo4gag954bIvB6EwLeGExDlOdYuzJUOrDdfcwFRemkzJs6XKBVrbiTvV6xrvXUyNOcpM5Y3r79fse7q9UtSIJASt7Q42P3gumJ5uTThiSgaHyvXvnr9y8OQPLD+tnaijkjrpcdNiapUgfDW7gWlIdXxbVja5KBE+VMFUIJp0GZoltIFSv2fmotbnVVCPWWWOIOAd4KbISDyPwQit4JNcDaZKxpXVwE5gSQ5mAKWEkSVGA2/JlMco6ESozGyktyIlD50NIYUKeUejSZx32AfDb10vAVZZh0NmVJmIFlW3ECU6NFOtWUeF7ndJZYsPWuCR+Q2SZZz7A09fkQTIG6beN6QVPmvQ3sDSpUh8cClqVJnCBySFEqp2A6Oo9ZQV7CTmbc2keCKAgQ+f9cwhgHFUqomJTgqwzEvao8zUecQtyQ1udXMnCa2oaG0PFGklNqqMIGUqnKDkFsdCKhU9pRq7vHWCKnUiYIX09a7iJSOJKX0Vq0PKF2bG0SNUkre4WbsOJThq6OCeKFJn08h3U92CJ2/Jy4vMXEJEnUM0pKOFwmc0Uu8CYDIwotiJRiXUo4n/h127/SO0H9G37l/9LwIrhbgzdyfYUrB3WDHDx2uNMVLmS8BBgAAaIRb/IXYdgAAAABJRU5ErkJggg==");

/***/ }),

/***/ "./assets/src/images/retaining-walls.svg":
/*!***********************************************!*\
  !*** ./assets/src/images/retaining-walls.svg ***!
  \***********************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = (__webpack_require__.p + "../images/retaining-walls.svg");

/***/ }),

/***/ "./assets/src/images/snow-removal.svg":
/*!********************************************!*\
  !*** ./assets/src/images/snow-removal.svg ***!
  \********************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = (__webpack_require__.p + "../images/snow-removal.svg");

/***/ }),

/***/ "./assets/src/images/tree-shrub-groundcare.svg":
/*!*****************************************************!*\
  !*** ./assets/src/images/tree-shrub-groundcare.svg ***!
  \*****************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = (__webpack_require__.p + "../images/tree-shrub-groundcare.svg");

/***/ }),

/***/ "./assets/src/js/gallery.js":
/*!**********************************!*\
  !*** ./assets/src/js/gallery.js ***!
  \**********************************/
/*! no static exports found */
/***/ (function(module, exports) {

(function ($) {
  window.wpBlockGallerySwiper = null;
  $(document).ready(function () {
    $(".wp-block-gallery a").click(function (e) {
      e.preventDefault();
    });
    $(".wp-block-gallery img").click(function () {
      //setup swiper if not existing
      if (window.wpBlockGallerySwiper == null) {
        var swiperHtml = '<div class="swiper-container swiper1"><div class="swiper-wrapper"></div><div class="swiper-pagination swiper-pagination-white"></div><div class="swiper-button-next swiper-button-white"></div><div class="swiper-button-prev swiper-button-white"></div></div>';
        $("body").append(swiperHtml);
        window.wpBlockGallerySwiper = new Swiper('.swiper1', {
          preloadImages: false,
          // Enable lazy loading
          lazy: true,
          pagination: {
            el: '.swiper-pagination',
            clickable: true
          },
          navigation: {
            nextEl: '.swiper-button-next',
            prevEl: '.swiper-button-prev'
          },
          watchSlidesVisibility: true
        });
      } else {
        window.wpBlockGallerySwiper.removeAllSlides();
      } //prepare slides from gallery


      var wpBlockGalleryEl = $(this).parents(".wp-block-gallery");
      var wpBlockGallerySlides = [];
      wpBlockGalleryEl.find('img').each(function (index) {
        wpBlockGallerySlides.push('<div class="swiper-slide"> <img data-srcset="' + $(this).attr('srcset') + '" data-sizes="' + $(this).attr('sizes') + '" data-src="' + $(this).attr('src') + '" class="swiper-lazy"/><div class="swiper-lazy-preloader swiper-lazy-preloader-white"></div></div>');
      });
      window.wpBlockGallerySwiper.addSlide(1, wpBlockGallerySlides);
      window.wpBlockGallerySwiper.lazy.load(); //window.wpBlockGallerySwiper.update();
    });
  });
})(jQuery);

/***/ }),

/***/ "./assets/src/js/script.js":
/*!*********************************!*\
  !*** ./assets/src/js/script.js ***!
  \*********************************/
/*! no exports provided */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _sass_style_scss__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ../sass/style.scss */ "./assets/src/sass/style.scss");
/* harmony import */ var _sass_style_scss__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_sass_style_scss__WEBPACK_IMPORTED_MODULE_0__);


__webpack_require__(/*! ./window-event/ready.js */ "./assets/src/js/window-event/ready.js");

__webpack_require__(/*! ./window-event/load.js */ "./assets/src/js/window-event/load.js");

__webpack_require__(/*! ./window-event/resize.js */ "./assets/src/js/window-event/resize.js");

__webpack_require__(/*! ./window-event/scroll.js */ "./assets/src/js/window-event/scroll.js");

__webpack_require__(/*! ./gallery.js */ "./assets/src/js/gallery.js");

/***/ }),

/***/ "./assets/src/js/window-event/load.js":
/*!********************************************!*\
  !*** ./assets/src/js/window-event/load.js ***!
  \********************************************/
/*! no static exports found */
/***/ (function(module, exports) {

// Windows Load Handler
(function ($) {
  $(window).on('load', function () {});
})(jQuery);

/***/ }),

/***/ "./assets/src/js/window-event/ready.js":
/*!*********************************************!*\
  !*** ./assets/src/js/window-event/ready.js ***!
  \*********************************************/
/*! no static exports found */
/***/ (function(module, exports) {

// Windows Ready Handler
(function ($) {
  $(document).ready(function () {
    $(".show4ServicesEl a").click(function (e) {
      e.preventDefault();
      var featuredServices = $(".our-featured-services__two");
      featuredServices.removeClass('d-none');
      $(this).hide();
      scrollTo();
    });
  });
})(jQuery);

/***/ }),

/***/ "./assets/src/js/window-event/resize.js":
/*!**********************************************!*\
  !*** ./assets/src/js/window-event/resize.js ***!
  \**********************************************/
/*! no static exports found */
/***/ (function(module, exports) {

// Window Resize Handler
(function ($) {
  $(window).on('resize', function () {});
})(jQuery);

/***/ }),

/***/ "./assets/src/js/window-event/scroll.js":
/*!**********************************************!*\
  !*** ./assets/src/js/window-event/scroll.js ***!
  \**********************************************/
/*! no static exports found */
/***/ (function(module, exports) {

// Window Scroll Handler
(function ($) {
  $(window).on('scroll', function () {});
})(jQuery);

/***/ }),

/***/ "./assets/src/sass/style.scss":
/*!************************************!*\
  !*** ./assets/src/sass/style.scss ***!
  \************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

// extracted by mini-css-extract-plugin

/***/ })

/******/ });
//# sourceMappingURL=main.js.map