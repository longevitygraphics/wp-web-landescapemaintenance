// Windows Ready Handler

(function ($) {

  $(document).ready(function () {
    $(".show4ServicesEl a").click(function (e) {
      e.preventDefault();
      var featuredServices = $(".our-featured-services__two");
      featuredServices.removeClass('d-none');
      $(this).hide();
      scrollTo()
    });
  });

}(jQuery));
